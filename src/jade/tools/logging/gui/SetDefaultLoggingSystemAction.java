package jade.tools.logging.gui;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

class SetDefaultLoggingSystemAction extends AbstractAction {
	private final LogManagerGUI gui;
	
	public SetDefaultLoggingSystemAction(LogManagerGUI gui) {
		super ("Set default logging system");
		this.gui = gui;
	}
	
	public void actionPerformed(ActionEvent e) {
		gui.setDefaultLoggingSystem();
	}
}
