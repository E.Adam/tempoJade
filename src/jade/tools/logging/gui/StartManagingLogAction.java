package jade.tools.logging.gui;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

class StartManagingLogAction extends AbstractAction {
	private final LogManagerGUI gui;
	
	public StartManagingLogAction(LogManagerGUI gui) {
		super ("Start Managing Log");
		this.gui = gui;
	}
	
	public void actionPerformed(ActionEvent e) {
		gui.startManagingLog();
	}
}
