package jade.tools.logging.gui;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

class ExitAction extends AbstractAction {
	private final LogManagerGUI gui;
	
	public ExitAction(LogManagerGUI gui) {
		super ("Exit");
		this.gui = gui;
	}
	
	public void actionPerformed(ActionEvent e) {
		gui.exit();
	}
}
