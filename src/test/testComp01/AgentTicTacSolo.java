package test.testComp01;

import static java.lang.System.out;

import jade.core.Agent;
import jade.core.behaviours.TickerBehaviour;
import jade.core.behaviours.WakerBehaviour;

public class AgentTicTacSolo extends Agent {
    @Override
    protected void setup() {
        addBehaviour(new WakerBehaviour(this, 2000) {
            public void onWake() {
                myAgent.doDelete();}
        });

        addBehaviour(new TickerBehaviour(this, 500) {
            public void onTick() {
                System.out.println(myAgent.getLocalName() + " tictac ");}
        });
    }

    @Override
    protected void takeDown() {
        out.println("Moi, Agent " + getLocalName() + " je quitte la plateforme ! ");
    }

    public static void main(String[] args)
    {
        String[] jadeArgs = new String[2];
        StringBuilder sbAgents = new StringBuilder();
        sbAgents.append("a1:test.testComp01.AgentTicTacSolo").append(";");
        sbAgents.append("a2:test.testComp01.AgentTicTacSolo").append(";");
        jadeArgs[0] = "-gui";
        jadeArgs[1] = sbAgents.toString();
        jade.Boot.main(jadeArgs);
    }
}

