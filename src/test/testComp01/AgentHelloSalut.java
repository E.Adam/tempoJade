package test.testComp01;

import static java.lang.System.out;

import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.CyclicBehaviour;
import jade.core.behaviours.WakerBehaviour;

/**
 * classe d'un agents qui contient 2 comportements sans fin, qui affichent l'un 'bonjour', l'autre 'salut'
 * l'agents possède aussi un comportement à allumage retardé qui le supprime de la plateforme
 * @author emmanueladam
 * */
public class AgentHelloSalut extends Agent {
	 /** Initialisation de l'agents */
	 int i=0;
	@Override
	 protected void setup() {
		out.println("Moi, Agent "+ getLocalName()+", mon  adresse est "+ getAID());

		// ajout d'un comportement cyclique qui, à chaque passage, affiche bonjour et fait une pause de 10 ms
		addBehaviour(new Behaviour(this) {
			public void action() {
				out.println("De l'agent " + getLocalName() + " : Bonjour BREF");
				myAgent.doWait(10);
			}

			@Override
			public boolean done() {
				return true;
			}
		});

		// ajout d'un comportement cyclique qui, à chaque passage, affiche salut et fait une pause de 10 ms
		addBehaviour(new CyclicBehaviour(this) {
			public void action() {
				out.println("De l'agent " + getLocalName() + " : Salut en boucle " + i);
				i++;
				myAgent.doWait(30);
			}
		});

		// ajout d'un comportement qui retire l'agents dans 1000 ms
		addBehaviour(new WakerBehaviour(this, 1000) {
			protected void onWake() {
				myAgent.doDelete();
			}
		});
	  }

	 // 'Nettoyage' de l'agents
	@Override
	 protected void takeDown() {
	  out.println("Moi, Agent " + getLocalName() + " je quitte la plateforme ! ");
	}

	/**procedure principale.
	 * lance 2 agents qui agissent en "parallele" et dont les comportements s'éxécutent dans le même cycle de temps
	 * */
	public static void main(String[] args)
	{
		String[] jadeArgs = new String[2];
		StringBuilder sbAgents = new StringBuilder();
		sbAgents.append("a1:test.testComp01.AgentHelloSalut").append(";");
		sbAgents.append("a2:test.testComp01.AgentHelloSalut").append(";");
		jadeArgs[0] = "-gui";
		jadeArgs[1] = sbAgents.toString();
		jade.Boot.main(jadeArgs);
	}
}