package test.AgentService.launch;


import java.util.Properties;

import jade.core.Profile;
import jade.core.ProfileImpl;
import jade.core.Runtime;
import jade.util.ExtendedProperties;

public class LaunchAgent {
    public static void main(String[] args) {
        // preparer les arguments pout le conteneur JADE
        Properties prop = new ExtendedProperties();
        // demander la fenetre de controle
        prop.setProperty(Profile.GUI, "true");
        // nommer les agents
//        prop.setProperty(Profile.AGENTS,"a1:AgentService.agents.HelloAgent(hello);a2:AgentService.agents.HelloAgent(salut);a3:AgentService.agents.HelloAgent(coucou)");
        //avec 100 agents :
        StringBuilder sb = new StringBuilder();
        String typeAgent = ":test.AgentService.agents.HelloAgent(hello);";
        for (int i = 0; i < 5; i++) sb.append("a").append(i).append(typeAgent);
        prop.setProperty(Profile.AGENTS, sb.toString());
        // creer le profile pour le conteneur principal
        ProfileImpl profMain = new ProfileImpl(prop);
        // lancer le conteneur principal
        Runtime rt = Runtime.instance();
        rt.createMainContainer(profMain);
    }
}