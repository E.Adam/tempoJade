package test.fenetre.agents;


import java.time.LocalDateTime;
import java.util.Iterator;

import jade.core.AID;
import jade.core.behaviours.DataStore;
import jade.lang.acl.ACLMessage;
import jade.proto.states.MsgReceiver;
import test.fenetre.gui.SimpleWindow4Agent;

/**
 * agent lié à une fenêtre qui attend des messages et les affiche
 *
 * @author eadam
 */
@SuppressWarnings("serial")
public class AgentReceptionComp extends AgentWindowed {


    /**
     * initialize the agent <br>
     * create the local dir to store data and roles <br>
     * add the stack of behaviours (pileComportements)
     */
    protected void setup() {
        window = new SimpleWindow4Agent(getAID().getName(), this);
        println("Hello! Agent  " + getAID().getName() + " is ready. ");

        DataStore ds = new DataStore();
        String key = LocalDateTime.now().toString();
        // comportement cyclique d'affichage de messages
        addBehaviour(new MsgReceiver(this, null, MsgReceiver.INFINITE,  ds, key) {
            @Override
            public void handleMessage(ACLMessage msg) {
                // recuperation du contenu :
                String contenu = msg.getContent();
                // recuperation de l'adresse de l'emetteur
                AID adresseEmetteur = msg.getSender();
                // recuperation du nom déclaré en local de l'emetteur
                String nomEmetteur = adresseEmetteur.getLocalName();

				println("recu " + contenu + ", de " + nomEmetteur);
				println("contenu du datastote :  ");
				Iterator<?> itr = ds.keySet().iterator();
				itr.forEachRemaining(k->println(k + " - " + ds.get(k)));

                String key = LocalDateTime.now().toString();
                reset(null, MsgReceiver.INFINITE, this.getDataStore(), key);
            }
        });

    }


    /**
     * @return the window
     */
    public SimpleWindow4Agent getWindow() {
        return window;
    }


    /**
     * @param window the window to set
     */
    public void setWindow(SimpleWindow4Agent window) {
        this.window = window;
    }


}
