package test.ticTac;

import java.util.Properties;

import jade.core.Agent;
import jade.core.Profile;
import jade.core.ProfileImpl;
import jade.core.Runtime;
import jade.util.ExtendedProperties;

/**
 * classe d'agents pour échange entre 2 agents de cette classe. l'un s'appelle ping et initie un échange avec l'agents pong.
 *
 * @author emmanueladam
 * */
public class LaunchAgents extends Agent {

    public static void main(String[] args) {
        // preparer les arguments pout le conteneur JADE
        Properties prop = new ExtendedProperties();
        // demander la fenetre de controle
        prop.setProperty(Profile.GUI, "true");
        // nommer les agents
        prop.setProperty(Profile.AGENTS, "agentDecompte:test.ticTac.AgentPosteur;agentPiege:test.ticTac.AgentLecteur");
        // creer le profile pour le conteneur principal
        ProfileImpl profMain = new ProfileImpl(prop);
        // lancer le conteneur principal
        Runtime rt = Runtime.instance();
        rt.createMainContainer(profMain);
    }
}