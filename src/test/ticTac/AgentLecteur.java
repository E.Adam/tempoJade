package test.ticTac;

import static java.lang.System.out;

import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.lang.acl.MessageTemplate;

/**
 * classe d'agents pour échange entre 2 agents de cette classe. l'un s'appelle ping et initie un échange avec l'agents pong.
 *
 * @author emmanueladam
 * */
public class AgentLecteur extends Agent {
    /**
     * Initialisation de l'agents
     */
    @Override
    protected void setup() {
        out.println("De l'agent " + getLocalName());

        // ajout d'un comportement qui attend des msgs de type clock
        addBehaviour(new CyclicBehaviour(this) {
            MessageTemplate mt = MessageTemplate.MatchConversationId("CLOCK");

            public void action() {
                var msg = receive(mt);
                if (msg != null) {
                    var content = msg.getContent();
                    var sender = msg.getSender();
                    out.println("agent " + getLocalName() + " : j'ai recu " + content + " de " + sender);
                } else block();
            }
        });
        // ajout d'un comportement qui attend des msgs de type boom
        addBehaviour(new CyclicBehaviour(this) {
            MessageTemplate mt = MessageTemplate.MatchConversationId("BOOM");

            public void action() {
                var msg = receive(mt);
                if (msg != null) {
                    var content = msg.getContent();
                    var sender = msg.getSender();
                    out.println("attention !!!! agent " + getLocalName() + " : j'ai recu " + content + " de " + sender);
                } else block();
            }
        });
    }

}