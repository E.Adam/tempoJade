package test.radio.launch;

import java.util.Properties;

import jade.core.Profile;
import jade.core.ProfileImpl;
import jade.core.Runtime;
import jade.util.ExtendedProperties;

public class LaunchAgents {
	 public static void main(String[] args) {
	  // preparer les arguments pout le conteneur JADE
	  Properties prop = new ExtendedProperties();
	  // demander la fenetre de controle
	  prop.setProperty(Profile.GUI, "true");
	  // add the Topic Management Service
	  prop.setProperty(Profile.SERVICES, "jade.core.messaging.TopicManagementService;jade.core.event.NotificationService");
	  // nommer les agents
	  prop.setProperty(Profile.AGENTS, "a:test.radio.agents.AgentDiffuseur;"
	  		+ "b:test.radio.agents.AgentAuditeur;c:test.radio.agents.AgentAuditeur;d:test.radio.agents.AgentAuditeur");
	  // creer le profile pour le conteneur principal
	  ProfileImpl profMain = new ProfileImpl(prop);
	  // lancer le conteneur principal
	  Runtime rt = Runtime.instance();
	  rt.createMainContainer(profMain);
	  }
	}
